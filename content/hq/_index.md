---
title: "HQ"
---

## Upgrade Costs

| HQ Level | Permits From -> To | Part Costs                                                                  | Queue Slots |
|----------|--------------------|-----------------------------------------------------------------------------|-------------|
| 2        | 2  -> 3            | 6 BBH, 4 BDE, 4 BSE, 2 BTA, 12 MCG, 4 TRU                                   | 5           |
| 3        | 3  -> 4            | 16 BBH, 12 BDE, 12 BSE, 6 BTA, 36 MCG, 12 TRU, 20 OFF                       | 5           |
| 4        | 4  -> 5            | 24 BBH, 16 BDE, 16 BSE, 8 BTA, 48 MCG, 8 MFK, 16 TRU, 4 UTS                 | 5           |
| 5        | 5  -> 6            | 40 OFF, 20 SUN, 8 UTS, 16 MFK                                               | 7           |
| 6        | 6  -> 7            | 6 LBH, 4 LDE, 4 LSE, 2 LTA, 24 MCG, 12 TRU, 10 POW                          | 7           |
| 7        | 7  -> 8            | 12 LBH, 8 LDE, 8 LSE, 4 LTA, 36 MCG, 16 TRU, 10 SP                          | 7           |
| 8        | 8  -> 9            | 16 LBH, 12 LDE, 12 LSE, 6 LTA, 48 MCG, 20 TRU, 60 OFF, 2 SP                 | 7           |
| 9        | 9  -> 10           | 24 LBH, 16 LDE, 16 LSE, 8 LTA, 60 MCG, 24 TRU, 10 SP, 10 POW                | 7           |
| 10       | 10 -> 11           | 100 OFF, 14 SP, 10 POW                                                      | 9           |
| 11       | 11 -> 12           | 6 RBH, 4 RDE, 4 RSE, 2 RTA, 48 MCG, 20 TRU, 4 AAR                           | 9           |
| 12       | 12 -> 13           | 12 RBH, 8 RDE, 8 RSE, 4 RTA, 60 MCG, 24 TRU, 2 BWS                          | 9           |
| 13       | 13 -> 14           | 200 OFF, 1 BWS, 1 BMF, 1 AAR                                                | 11          |
| 14       | 14 -> 15           | 16 RBH, 12 RDE, 12 RSE, 6 RTA, 100 MCG, 28 TRU, 2 BWS, 2 AAR                | 11          |
| 15       | 15 -> 16           | 24 RBH, 16 RDE, 16 RSE, 8 RTA, 150 MCG, 32 TRU, 2 BWS, 2 BMF                | 11          |
| 16       | 16 -> 17           | 300 OFF, 1 LOG                                                              | 14          |
| 17       | 17 -> 18           | 12 ABH, 8 ADE, 8 ASE, 4 ATA, 200 MCG, 40 TRU, 1 COM                         | 14          |
| 18       | 18 -> 19           | 1 ADS, 400 OFF                                                              | 17          |
| 19       | 19 -> 20           | 24 ABH, 16 ADE, 1 ADS, 16 ASE, 8 ATA, 1 COM, 1 LOG, 500 MCG, 80 TRU         | 17          |
| 20       | 20 -> 21           | 1 ADS, 1 COM, 1 LOG, 500 OFF                                                | 20          |
| 21       | 21 -> 22           | 51 ABH, 34 ADE, 2 ADS, 34 ASE, 17 ATA, 2 COM, 2 LOG, 214 MCG, 85 TRU        | 20          |
| 22       | 22 -> 23           | 79 ABH, 53 ADE, 2 ADS, 53 ASE, 26 ATA, 2 COM, 2 LOG, 331 MCG, 132 TRU       | 20          |
| 23       | 23 -> 24           | 108 ABH, 72 ADE, 3 ADS, 72 ASE, 36 ATA, 3 COM, 3 LOG, 451 MCG, 180 TRU      | 20          |
| 24       | 24 -> 25           | 137 ABH, 91 ADE, 3 ADS, 91 ASE, 45 ATA, 3 COM, 3 LOG, 574 MCG, 229 TRU      | 20          |
| 25       | 25 -> 26           | 168 ABH, 112 ADE, 3 ADS, 112 ASE, 56 ATA, 3 COM, 3 LOG, 700 MCG, 280 TRU    | 20          |
| 26       | 26 -> 27           | 199 ABH, 132 ADE, 3 ADS, 132 ASE, 66 ATA, 3 COM, 3 LOG, 829 MCG, 331 TRU    | 20          |
| 27       | 27 -> 28           | 231 ABH, 154 ADE, 3 ADS, 154 ASE, 77 ATA, 3 COM, 3 LOG, 963 MCG, 385 TRU    | 20          |
| 28       | 28 -> 29           | 264 ABH, 176 ADE, 3 ADS, 176 ASE, 88 ATA, 3 COM, 3 LOG, 1103 MCG, 441 TRU   | 20          |
| 29       | 29 -> 30           | 299 ABH, 199 ADE, 3 ADS, 199 ASE, 99 ATA, 3 COM, 3 LOG, 1248 MCG, 499 TRU   | 20          |
| 30       | 30 -> 31           | 336 ABH, 224 ADE, 4 ADS, 224 ASE, 112 ATA, 4 COM, 4 LOG, 1400 MCG, 560 TRU  | 20          |
| 31       | 31 -> 32           | 374 ABH, 249 ADE, 4 ADS, 249 ASE, 124 ATA, 4 COM, 4 LOG, 1559 MCG, 623 TRU  | 20          |
| 32       | 32 -> 33           | 414 ABH, 276 ADE, 4 ADS, 276 ASE, 138 ATA, 4 COM, 4 LOG, 1727 MCG, 691 TRU  | 20          |
| 33       | 33 -> 34           | 457 ABH, 305 ADE, 4 ADS, 305 ASE, 152 ATA, 4 COM, 4 LOG, 1906 MCG, 732 TRU  | 20          |
| 34       | 34 -> 35           | 503 ABH, 335 ADE, 4 ADS, 335 ASE, 167 ATA, 4 COM, 4 LOG, 2096 MCG, 838 TRU  | 20          |
| 35       | 35 -> 36           | 552 ABH, 368 ADE, 4 ADS, 368 ASE, 184 ATA, 4 COM, 4 LOG, 2300 MCG, 920 TRU  | 20          |
| 36       | 36 -> 37           | 604 ABH, 403 ADE, 4 ADS, 403 ASE, 201 ATA, 4 COM, 4 LOG, 2518 MCG, 1007 TRU | 20          |
| 37       | 37 -> 38           | 661 ABH, 440 ADE, 4 ADS, 440 ASE, 220 ATA, 4 COM, 4 LOG, 2755 MCG, 1102 TRU | 20          |
| 38       | 38 -> 39           | 723 ABH, 482 ADE, 5 ADS, 482 ASE, 241 ATA, 5 COM, 5 LOG, 3012 MCG, 1205 TRU | 20          |
| 39       | 39 -> 40           | 790 ABH, 526 ADE, 5 ADS, 526 ASE, 263 ATA, 5 COM, 5 LOG, 3292 MCG, 1317 TRU | 20          |
| 40       | 40 -> 41           | 864 ABH, 576 ADE, 5 ADS, 576 ASE, 288 ATA, 5 COM, 5 LOG, 3600 MCG, 1440 TRU | 20          |
| 41       | 41 -> 42           | 945 ABH, 630 ADE, 5 ADS, 630 ASE, 315 ATA, 5 COM, 5 LOG, 3937 MCG, 1575 TRU | 20          |

## HQ Bonus 

The multiplier to the base efficiency bonus provided by the HQ can be given by:

`Multiplier = -2 * (UsedPermits / TotalPermits) + 3`
